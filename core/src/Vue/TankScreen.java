package Vue;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import game.MyGdxGame;
import Controler.WorldRenderer;

public class TankScreen implements Screen {
	private static boolean afficherScore = true;
	private SpriteBatch batch = new SpriteBatch();
	MyGdxGame jeu;
	WorldRenderer controleur;

	private BitmapFont text = new BitmapFont();

	public TankScreen(WorldRenderer controler, MyGdxGame jeu) {
		this.jeu = jeu;
		this.controleur = controler;
	}

	public void render(float delta) {
		Gdx.gl.glClear(16640);
		Gdx.gl.glClearColor(1.0F, 1.0F, 1.0F, 1.0F);
		this.batch.begin();
		this.controleur.render(delta, this.batch);
		this.text.setColor(Color.WHITE);
		this.text.draw(this.batch, "vie : " + this.controleur.getVie(), 20.0F, 20.0F);
		this.text.draw(this.batch,"nombre de Récompenses : " + this.controleur.getnbOiseaux(), 200.0F,20.0F);
		this.text.draw(this.batch,"=> Prends le Phoenix après les 3 Récompenses ", 530.0F,20.0F);
		/* if(World.getNbPhoenix()==0){
			this.jeu.setScreen(new EcranFinDeJeu(this.jeu, this.controleur.getScore()));
		}
		*/

		if(this.controleur.end()){
			this.jeu.setScreen(new EcranFinDeJeu(this.jeu, this.controleur.getScore()));
		}
		this.batch.end();
	}

	public void show() {
	}

	public void resize(int width, int height) {
	}

	public void pause() {
	}

	public void resume() {
	}

	public void hide() {
	}

	public void dispose() {
	}
}
