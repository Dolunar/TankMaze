package Vue;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public class TextureFactory {
	private static TextureFactory singleton;
	private Texture vegetation;
	private Texture[] charEnnemi;
	private Texture fond;
	private Texture murBrique;
	private Texture murFer;
	private Texture oiseau;
	private Texture[] bullet;
	private Texture[][] myChar;
	private Texture[] explosions;
	private Texture avion;
	private Texture phoenix;

	public static TextureFactory getInstance() {
		if (singleton == null) {
			singleton = new TextureFactory();
		}

		return singleton;
	}

	private TextureFactory() {
		this.phoenix = new Texture(Gdx.files.internal("core/assets/phoenix.png"));
		this.vegetation = new Texture(Gdx.files.internal("core/assets/vegetation.png"));
		this.myChar = new Texture[4][8];
		this.fond = new Texture(Gdx.files.internal("core/assets/fond.png"));
		this.murBrique = new Texture(Gdx.files.internal("core/assets/murBrique.png"));
		this.murFer = new Texture(Gdx.files.internal("core/assets/murFer.png"));
		this.oiseau = new Texture(Gdx.files.internal("core/assets/oiseau.png"));
		this.avion = new Texture(Gdx.files.internal("core/assets/avion.png"));
		this.bullet = new Texture[4];
		this.bullet[0] = new Texture(Gdx.files.internal("core/assets/balleDroite.png"));
		this.bullet[1] = new Texture(Gdx.files.internal("core/assets/balleGauche.png"));
		this.bullet[2] = new Texture(Gdx.files.internal("core/assets/balleHaut.png"));
		this.bullet[3] = new Texture(Gdx.files.internal("core/assets/balleBas.png"));
		this.charEnnemi = new Texture[4];
		this.charEnnemi[0] = new Texture(Gdx.files.internal("core/assets/charEnnemiDroite.png"));
		this.charEnnemi[1] = new Texture(Gdx.files.internal("core/assets/charEnnemiGauche.png"));
		this.charEnnemi[2] = new Texture(Gdx.files.internal("core/assets/charEnnemiHaut.png"));
		this.charEnnemi[3] = new Texture(Gdx.files.internal("core/assets/charEnnemiBas.png"));
		this.explosions = new Texture[3];
		this.explosions[0] = new Texture(Gdx.files.internal("core/assets/explo1.png"));
		this.explosions[1] = new Texture(Gdx.files.internal("core/assets/explo2.png"));
		this.explosions[2] = new Texture(Gdx.files.internal("core/assets/explo3.png"));
		String[] sens = new String[]{"Droite", "Gauche", "Haut", "Bas"};

		for(int i = 0; i < 4; ++i) {
			this.myChar[i] = new Texture[8];

			for(int j = 0; j < 8; ++j) {
				this.myChar[i][j] = new Texture(Gdx.files.internal("core/assets/monChar/monChar" + sens[i] + (j + 1) + ".png"));
			}
		}

	}

	public Texture getVegetation() {
		return this.vegetation;
	}

	public Texture charEnnemi(int dirrection) {
		if (dirrection == 0) {
			dirrection = 1;
		}

		return this.charEnnemi[dirrection - 1];
	}

	public Texture getFond() {
		return this.fond;
	}

	public Texture getMurBrique() {
		return this.murBrique;
	}

	public Texture getMurFer() {
		return this.murFer;
	}

	public Texture getOiseau() {
		return this.oiseau;
	}

	public Texture getPhoenix(){
		return this.phoenix;
	}

	public Texture getBullet(int dirrection) {
		if (dirrection == 0) {
			dirrection = 1;
		}

		return this.bullet[dirrection - 1];
	}

	public Texture getMonChar(int direction, float compteur) {
		return this.myChar[direction - 1][(int)(compteur * 5.0F) % 8];
	}

	public Texture getExplosion(float animation) {
		int index = (int)(animation * 3.0F);
		return this.explosions[index];
	}

	public Texture getAvion() {
		return this.avion;
	}

}









