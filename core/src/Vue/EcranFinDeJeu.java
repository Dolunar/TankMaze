package Vue;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import Modele.World;
import game.MyGdxGame;

import Controler.WorldRenderer;

public class EcranFinDeJeu implements Screen {
	private BitmapFont text = new BitmapFont();
	private SpriteBatch batch = new SpriteBatch();
	private int score;

	MyGdxGame game;

	World world;

	public EcranFinDeJeu(MyGdxGame jeu, int score) {
		this.score = score;
	}


	public void show() {
	}

	public void render(float delta){
		this.batch.begin();
		Gdx.gl.glClear(16640);
		Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
		this.text.setColor(Color.RED);
		this.text.draw(this.batch, "Fin de partie, votre score : " + this.score, 400.0F, 300.0F);
		this.text.draw(this.batch, "Merci Beaucoup d'avoir joué à mon jeu =) : ",400.0F, 200.0F);
		this.text.draw(this.batch, "Signé Dolu", 400.0F, 100.0F);

		this.batch.end();
	}


	public void resize(int width, int height) {
	}

	public void pause() {
	}

	public void resume() {
	}

	public void hide() {
	}

	public void dispose() {
	}
}
