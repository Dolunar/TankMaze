//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package Controler;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import game.MyGdxGame;
import java.util.ArrayList;
import java.util.Iterator;
import Modele.World;
import Modele.*;
import Modele.ElementDeJeuStatique.*;
import Vue.*;

public class WorldRenderer implements InputProcessor {
	private static float delta;
	private World modele;
	private int dirrection;
	private float compteur;

	public static float DELTA() {
		return delta;
	}

	public WorldRenderer(World monde, MyGdxGame jeu) {
		this.modele = monde;
		this.dirrection = 0;
		this.compteur = 0.0F;
	}

	public void render(float delta, SpriteBatch batch) {
		WorldRenderer.delta = delta;
		if (this.dirrection != 0) {
			this.compteur += delta;
		}

		Char monChar = this.modele.getMonChar();
		this.modele.avancerEnnemis(delta);
		this.modele.avancerBalles();
		this.modele.animerExplosions(delta);
		this.modele.avancerAvion(delta);
		this.modele.oiseau();
		this.modele.Phoenix();
		Iterator var5 = this.getCases().iterator();

		ArrayList explo;

		//TEXTURES MAP
		while(var5.hasNext()) {
			explo = (ArrayList)var5.next();
			Iterator var7 = explo.iterator();

			while(var7.hasNext()) {
				ElementDeJeuStatique c = (ElementDeJeuStatique)var7.next();
				if (c instanceof Fond) {
					batch.draw(TextureFactory.getInstance().getFond(), (float)((int)c.getX()), (float)((int)c.getY()), (float)c.getTaille(), (float)c.getTaille());
				} else if (c instanceof MurBrique) {
					batch.draw(TextureFactory.getInstance().getMurBrique(), (float)((int)c.getX()), (float)((int)c.getY()), (float)c.getTaille(), (float)c.getTaille());
				} else if (c instanceof MurFer) {
					batch.draw(TextureFactory.getInstance().getMurFer(), (float)((int)c.getX()), (float)((int)c.getY()), (float)c.getTaille(), (float)c.getTaille());
				} else {
					batch.draw(TextureFactory.getInstance().getVegetation(), (float)((int)c.getX()), (float)((int)c.getY()), (float)c.getTaille(), (float)c.getTaille());
				}
			}
		}

		//BALLES
		var5 = this.getBalle().iterator();

		while(var5.hasNext()) {
			Balle b = (Balle)var5.next();
			batch.draw(TextureFactory.getInstance().getBullet(b.getDirrection()), (float)((int)b.getX()), (float)((int)b.getY()), (float)b.getTaille(), (float)b.getTaille());
		}

		//MON CHAR
		batch.draw(TextureFactory.getInstance().getMonChar(this.getMonChar().getDirrection(), this.compteur), (float)((int)this.getMonChar().getX()), (float)((int)this.getMonChar().getY()), (float)this.getMonChar().getTaille(), (float)this.getMonChar().getTaille());
		var5 = this.getEnnemis().iterator();

		while(var5.hasNext()) {
			Char c = (Char)var5.next();
			batch.draw(TextureFactory.getInstance().charEnnemi(c.getDirrection()), (float)((int)c.getX()), (float)((int)c.getY()), (float)c.getTaille(), (float)c.getTaille());
		}
		//EXPLOSION :

		explo = this.modele.getExplosion();
		Iterator var12 = explo.iterator();
		while(var12.hasNext()) {
			Explosion e = (Explosion)var12.next();
			batch.draw(TextureFactory.getInstance().getExplosion(e.getAnimation()), (float)((int)e.getX()), (float)((int)e.getY()), (float)e.getTaille(), (float)e.getTaille());
		}

		//AVION / SOUCOUPE

		Avion a = this.modele.getAvion();
		batch.draw(TextureFactory.getInstance().getAvion(), (float)((int)a.getX()), (float)((int)a.getY()), (float)a.getTaille(), (float)a.getTaille());

		//mes ZOIZOSSSS
		ArrayList<Oiseau> oiseaux = this.modele.getOiseau();
		Iterator var8 = oiseaux.iterator();
		while(var8.hasNext()) {
			Oiseau o = (Oiseau)var8.next();
			batch.draw(TextureFactory.getInstance().getOiseau(), (float)((int)o.getX()), (float)((int)o.getY()), (float)o.getTaille(), (float)o.getTaille());
		}

		//THEEEEEE PHOENIXES
		ArrayList<Phoenix> phoenixes = this.modele.getPhoenix();
		Iterator var15 = phoenixes.iterator();
		while (var15.hasNext()){
			Phoenix p = (Phoenix)var15.next();
			batch.draw(TextureFactory.getInstance().getPhoenix(), (float)((int)p.getX()), (float)((int)p.getY()), (float)p.getTaille(),(float)p.getTaille());
		}

		switch (this.dirrection) {
			case 1:
				monChar.deplacerDroite();
				if (this.modele.estDansMur(monChar) || this.modele.getCharDans(monChar) != null) {
					monChar.deplacerGauche();
				}
				break;
			case 2:
				monChar.deplacerGauche();
				if (this.modele.estDansMur(monChar) || this.modele.getCharDans(monChar) != null) {
					monChar.deplacerDroite();
				}
				break;
			case 3:
				monChar.deplacerHaut();
				if (this.modele.estDansMur(monChar) || this.modele.getCharDans(monChar) != null) {
					monChar.deplacerBas();
				}
				break;
			case 4:
				monChar.deplacerBas();
				if (this.modele.estDansMur(monChar) || this.modele.getCharDans(monChar) != null) {
					monChar.deplacerHaut();
				}
		}

	}

	public ArrayList<ArrayList<ElementDeJeuStatique>> getCases() {
		return this.modele.getCases();
	}

	public Char getMonChar() {
		return this.modele.getMonChar();
	}

	public ArrayList<Balle> getBalle() {
		return this.modele.getBalles();
	}

	public ArrayList<CharEnnemi> getEnnemis() {
		return this.modele.getEnnemis();
	}

	public boolean keyDown(int keycode) {
		switch(keycode) {
			case 32:
				dirrection = 1;
				break;
			case 29:
				dirrection = 2;
				break;
			case 51:
				dirrection = 3;
				break;
			case 47:
				dirrection = 4;
				break;
			case 62:
				modele.tirer(modele.getMonChar(), true);
		}
		return false;

	}

	@Override
	public boolean keyUp(int keycode) {
		switch(keycode) {
			case 32:
				dirrection = 0;
				break;
			case 29:
				dirrection = 0;
				break;
			case 51:
				dirrection = 0;
				break;
			case 47:
				dirrection = 0;
				break;
		}
		return true;
	}


	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchCancelled(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	public boolean keyTyped(char character) {
		return false;
	}

	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(float amountX, float amountY) {
		return false;
	}

	public boolean scrolled(int amount) {
		return false;
	}

	public boolean end() {
		return this.modele.end();
	}

	public int getScore() {
		return this.modele.getScore();
	}

	public int getVie() {
		return this.modele.getVie();
	}

	public int getnbOiseaux(){return this.modele.getNbOiseaux();}

	public int getnbPhoenix(){
		return this.modele.getNbPhoenix();
	}
}
