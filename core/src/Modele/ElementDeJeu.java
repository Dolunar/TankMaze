package Modele;

public abstract class ElementDeJeu {

	protected double x;
	protected double y;
	protected int taille;

	public ElementDeJeu(double x, double y, int taille) {
		this.x = x;
		this.y = y;
		this.taille = taille;
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public int getTaille() {
		return this.taille;
	}

	public boolean estDedans(double x, double y) {
		return x >= this.getX() && x <= this.getX() + (double)this.getTaille() && y >= this.getY() && y <= this.getY() + (double)this.getTaille();
	}

	public void setPos(double x, double y) {
		this.x = x;
		this.y = y;
	}
}
