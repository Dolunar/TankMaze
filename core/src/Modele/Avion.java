package Modele;

import java.util.Random;

public class Avion extends ElementDeJeuLineaire{

	private float interval;
	private float attente; // -1 : en cours | 0 : pret
	private boolean enCours;
	private double v;
	private World world;
	private Random r;
	
	
	public Avion(int taille, double vitesse, float interval, World world) {
		super(-100, -100, 0, 0, taille);
		// TODO Auto-generated constructor stub
		attente = interval;
		this.interval = interval;
		this.world = world;
		enCours = false;
		r = new Random();
		v = vitesse;
	}
	
	
	public void avancer(float delta) {
		if(attente <= 0 && !enCours) {
			enCours = true;
			double destx = 0;
			double desty = 0;
			switch(r.nextInt(4)) {
			case 0: // de gauche � droite
				y = (r.nextInt(world.getM()-2)+1)* world.getTailleCase();
				x = 0;
				destx = world.getN()*world.getTailleCase();
				desty = y;
				break;
			case 1: // de droite � gauche
				y = (r.nextInt(world.getM()-2)+1)* world.getTailleCase();
				x = world.getN()*world.getTailleCase();
				destx = -world.getTailleCase();
				desty = y;
				break;
			case 2: // de haut en bas
				y = world.getM()*world.getTailleCase();
				x = (r.nextInt(world.getN()-2)+1)* world.getTailleCase();
				destx = x;
				desty = -(world.getTailleCase());
				break;
			case 3: // de bas en haut
				y = 0;
				x = (r.nextInt(world.getN()-2)+1)* world.getTailleCase();
				destx = x;
				desty = (world.getTailleCase())*world.getM();
				break;
			}
			
			double vp = Math.sqrt(destx*destx + desty*desty);
			
			vitessex = (destx-x) * v / 100;
			vitessey = (desty-y) * v / 100;
		}
		else if(!enCours)
			attente -= delta;
		else if(x < -world.getTailleCase() || y < -world.getTailleCase() || x > world.getM() * world.getTailleCase() || y > world.getN() * world.getTailleCase()) {
			attente = interval;
			enCours = false;
		}
		else {
			x += vitessex * delta;
			y += vitessey * delta;
		}
		
	}

}
