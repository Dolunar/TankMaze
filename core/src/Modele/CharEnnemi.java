package Modele;

public abstract class CharEnnemi extends Char{

	private float dernierTir = 1.0F;

	public CharEnnemi(double x, double y, double vitesse, int taille) {
		super(x, y, vitesse, taille);
	}

	public abstract void avancer(Char var1);

	public boolean tir(float delta, ElementDeJeu cible) {
		if (this.dernierTir > 0.0F) {
			this.dernierTir -= delta;
			return false;
		} else {
			switch (this.dirrection) {
				case 1:
					if (cible.getX() > this.getX() && cible.getY() < this.getY() + (double)this.getTaille() && cible.getY() + (double)this.getTaille() > this.getY()) {
						this.dernierTir = 1.0F;
						return true;
					}
					break;
				case 2:
					if (cible.getX() < this.getX() && cible.getY() < this.getY() + (double)this.getTaille() && cible.getY() + (double)this.getTaille() > this.getY()) {
						this.dernierTir = 1.0F;
						return true;
					}
					break;
				case 3:
					if (cible.getY() > this.getY() && cible.getX() < this.getX() + (double)this.getTaille() && cible.getX() + (double)this.getTaille() > this.getX()) {
						this.dernierTir = 1.0F;
						return true;
					}
					break;
				case 4:
					if (cible.getY() < this.getY() && cible.getX() < this.getX() + (double)this.getTaille() && cible.getX() + (double)this.getTaille() > this.getX()) {
						this.dernierTir = 1.0F;
						return true;
					}
			}

			return false;
		}
	}
}
