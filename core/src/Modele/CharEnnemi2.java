package Modele;

import java.util.Random;
import Modele.World;

public class CharEnnemi2 extends CharEnnemi {
	private World world;
	private int sens;

	public CharEnnemi2(double x, double y, double vitesse, int taille, World world) {
		super(x, y, vitesse, taille);
		this.world = world;
		this.sens = (new Random()).nextInt(4) + 1;
	}

	public void avancer(Char joueur) {
		switch (this.sens) {
			case 1:
				this.deplacerDroite();
				if (this.world.estDansMur(this)) {
					this.deplacerGauche();
					this.sens = (new Random()).nextInt(4) + 1;
				}
				break;
			case 2:
				this.deplacerGauche();
				if (this.world.estDansMur(this)) {
					this.deplacerDroite();
					this.sens = (new Random()).nextInt(4) + 1;
				}
				break;
			case 3:
				this.deplacerHaut();
				if (this.world.estDansMur(this)) {
					this.deplacerBas();
					this.sens = (new Random()).nextInt(4) + 1;
				}
				break;
			case 4:
				this.deplacerBas();
				if (this.world.estDansMur(this)) {
					this.deplacerHaut();
					this.sens = (new Random()).nextInt(4) + 1;
				}
		}

	}
}
