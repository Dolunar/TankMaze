package Modele;

import Controler.WorldRenderer;

public class ElementDeJeuLineaire extends ElementDeJeu {
	protected double vitessex;
	protected double vitessey;
	
	public ElementDeJeuLineaire(double x, double y, double vitessex, double vitessey, int taille) {
		super(x, y, taille);
		this.vitessex = vitessex;
		this.vitessey = vitessey;
	}
	
	
	public void avancer() {
		x += vitessex * WorldRenderer.DELTA();
		y += vitessey * WorldRenderer.DELTA();
	}

}
