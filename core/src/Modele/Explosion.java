package Modele;

public class Explosion extends ElementDeJeu {

	private double duration;
	private double animation;
	
	public Explosion(double x, double y, int taille, double duration) {
		super(x, y, taille);
		this.duration = duration;
		animation = 0;
	}
	
	
	public boolean animer(float delta) {
		animation += delta;
		return animation >= duration;
	}
	
	public float getAnimation() {
		return (float) (animation/duration);
	}
	

}
