//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package Modele;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;

import Controler.WorldRenderer;
import Modele.*;
import Modele.ElementDeJeuStatique.*;
import Vue.EcranFinDeJeu;
import game.MyGdxGame;

public class World {

	WorldRenderer controleur;
	MyGdxGame jeu;
	private static boolean ennemiContinu = true;
	private int scoreEnnemi;
	private int scoreOiseau;
	private int N;
	private int M;
	private int vie;
	private float tempsTir;
	private float tempsTirMax;
	private int score;
	private int nbPhoenix;
	private ArrayList<Oiseau> oiseaux = new ArrayList();
	private ArrayList<Phoenix> phoenixes = new ArrayList();
	private int nbOiseaux;
	private ArrayList<ArrayList<ElementDeJeuStatique>> cases = new ArrayList();
	private int tailleCase;
	private ArrayList<Balle> balles = new ArrayList();
	private double vitesseBalle;
	private int nbMaxBalles;
	private Char monChar;
	private ArrayList<CharEnnemi> ennemis = new ArrayList();
	private ArrayList<CharEnnemi> ennemisMorts = new ArrayList();
	private ArrayList<Explosion> explosions = new ArrayList();
	private Avion avion;
	private boolean finPartie;
	private Phoenix phoenix;

	public int getN() {
		return this.N;
	}

	public int getM() {
		return this.M;
	}

	public int getTailleCase() {
		return this.tailleCase;
	}

	public World(String config) {
		lireFichier("core/assets/config.txt");
	}

	public void setRandomCoord(ElementDeJeu e) {
		Random r = new Random();
		double x = (double)((r.nextInt(this.N - 2) + 1) * this.tailleCase);
		double y = (double)((r.nextInt(this.N - 2) + 1) * this.tailleCase);
		e.setPos(x, y);
	}

	public Avion getAvion() {
		return this.avion;
	}

	public void oiseau() {
		for(int i = 0; i < this.oiseaux.size(); ++i) {
			Oiseau oiseau = (Oiseau)this.oiseaux.get(i);
			if (oiseau.estDedans(this.monChar.x, this.monChar.y) || this.monChar.estDedans(oiseau.getX(), oiseau.getY())) {
				this.oiseaux.remove(oiseau);
				this.augmenterScore(this.scoreOiseau);
				--i;
				nbOiseaux--;
			}
		}
	}

	public void Phoenix(){
		if(this.oiseaux.size()==0) {
			for (int i = 0; i < this.phoenixes.size(); ++i) {
				Phoenix phoenix1 = (Phoenix) this.phoenixes.get(i);
				if (phoenix1.estDedans(this.monChar.x, this.monChar.y) || this.monChar.estDedans(phoenix1.getX(), phoenix1.getY())) {
					this.oiseaux.remove(phoenix1);
					this.augmenterScore(this.scoreOiseau);
					--i;
					this.jeu.setScreen(new EcranFinDeJeu(this.jeu, 999999999));
				}
			}
		}
	}

	public ArrayList<Oiseau> getOiseau() {
		return this.oiseaux;
	}

	public ArrayList<Phoenix> getPhoenix(){return this.phoenixes;}

	public int getNbOiseaux() {
		return nbOiseaux;
	}

	public int getNbPhoenix(){
		return nbPhoenix;
	}

	public void augmenterScore(int s) {
		this.score += s;
	}

	public int getScore() {
		return this.score;
	}

	public int getVie() {
		return this.vie;
	}

	public ArrayList<ArrayList<ElementDeJeuStatique>> getCases() {
		return this.cases;
	}

	public Char getMonChar() {
		return this.monChar;
	}

	public ArrayList<Balle> getBalles() {
		return this.balles;
	}

	public ArrayList<CharEnnemi> getEnnemis() {
		return this.ennemis;
	}

	public void avancerEnnemis(float delta) {
		this.tempsTir -= delta;
		if (this.tempsTir < 0.0F) {
			for(int i = 0; i < this.ennemis.size(); ++i) {
				this.tirer((Char)this.ennemis.get(i), false);
			}

			this.tempsTir = this.tempsTirMax;
		}

		new Random();
		Iterator var4;
		if (this.ennemis.size() == 0 && !ennemiContinu) {
			var4 = this.ennemisMorts.iterator();

			while(var4.hasNext()) {
				Char c = (Char)var4.next();
				this.setRandomCoord(c);
			}

			this.ennemis = new ArrayList(this.ennemisMorts);
			this.ennemisMorts.clear();
		}

		var4 = this.ennemis.iterator();

		while(var4.hasNext()) {
			CharEnnemi e = (CharEnnemi)var4.next();
			e.avancer(this.monChar);
			if (e.tir(delta, this.monChar)) {
				this.tirer(e, false);
			}
		}

	}

	public void animerExplosions(float delta) {
		for(int i = 0; i < this.explosions.size(); ++i) {
			if (((Explosion)this.explosions.get(i)).animer(delta)) {
				this.explosions.remove(i);
				--i;
			}
		}

	}

	public void avancerAvion(float delta) {
		this.avion.avancer(delta);
	}

	public ArrayList<Explosion> getExplosion() {
		return this.explosions;
	}

	public void avancerBalles() {
		for(int i = 0; i < this.balles.size(); ++i) {
			((Balle)this.balles.get(i)).avancer();
			Char c = this.getCharDans((ElementDeJeu)this.balles.get(i));
			if (c != null) {
				if (((Balle)this.balles.get(i)).gentille()) {
					if (ennemiContinu) {
						this.setRandomCoord(c);
					} else {
						this.ennemis.remove(c);
						this.ennemisMorts.add((CharEnnemi)c);
					}

					this.augmenterScore(this.scoreEnnemi);
					this.explosions.add(new Explosion(((Balle)this.balles.get(i)).getX(), ((Balle)this.balles.get(i)).getY(), (int)((double)((Balle)this.balles.get(i)).getTaille() * 1.5), 0.3));
					this.balles.remove(i);
					--i;
				}
			} else if (!((Balle)this.balles.get(i)).gentille() && this.monChar.estDedans(((Balle)this.balles.get(i)).x, ((Balle)this.balles.get(i)).y)) {
				--this.vie;
				if (this.vie == 0) {
					this.finPartie = true;
				}

				this.setRandomCoord(this.monChar);
				this.explosions.add(new Explosion(((Balle)this.balles.get(i)).getX(), ((Balle)this.balles.get(i)).getY(), (int)((double)((Balle)this.balles.get(i)).getTaille() * 1.5), 0.3));
				this.balles.remove(i);
				--i;
			} else if (this.getMur((ElementDeJeu)this.balles.get(i), ((Balle)this.balles.get(i)).gentille()) != null) {
				this.explosions.add(new Explosion(((Balle)this.balles.get(i)).getX(), ((Balle)this.balles.get(i)).getY(), (int)((double)((Balle)this.balles.get(i)).getTaille() * 1.5), 0.3));
				this.balles.remove(i);
				--i;
			}
		}

	}

	public void tirer(Char tireur, boolean gentille) {
		if (this.balles.size() < this.nbMaxBalles) {
			int dirrection = tireur.getDirrection();
			double vx = 0.0;
			double vy = 0.0;
			if (dirrection == 1) {
				vx = 1.0;
			} else if (dirrection == 2) {
				vx = -1.0;
			} else {
				vx = 0.0;
			}

			if (dirrection == 3) {
				vy = 1.0;
			} else if (dirrection == 4) {
				vy = -1.0;
			} else {
				vy = 0.0;
			}

			this.balles.add(new Balle(tireur.getX() + (double)(this.tailleCase / 2) - (double)(this.tailleCase / 4), tireur.getY() + (double)(this.tailleCase / 2) - (double)(this.tailleCase / 4), vx * this.vitesseBalle, vy * this.vitesseBalle, this.tailleCase / 2, gentille));
		}

	}

	public boolean estDansMur(ElementDeJeu e) {
		Iterator var3 = this.cases.iterator();

		while(var3.hasNext()) {
			ArrayList<ElementDeJeuStatique> ligne = (ArrayList)var3.next();
			Iterator var5 = ligne.iterator();

			while(var5.hasNext()) {
				ElementDeJeuStatique c = (ElementDeJeuStatique)var5.next();
				if (c.estDedans(e.getX(), e.getY()) || c.estDedans(e.getX() + (double)e.getTaille(), e.getY()) || c.estDedans(e.getX(), e.getY() + (double)e.getTaille()) || c.estDedans(e.getX() + (double)e.getTaille(), e.getY() + (double)e.getTaille())) {
					return true;
				}
			}
		}

		return false;
	}

	public ElementDeJeuStatique getMur(ElementDeJeu e, boolean destruction) {
		for(int i = 0; i < this.cases.size(); ++i) {
			ArrayList<ElementDeJeuStatique> ligne = (ArrayList)this.cases.get(i);

			for(int j = 0; j < ligne.size(); ++j) {
				ElementDeJeuStatique c = (ElementDeJeuStatique)ligne.get(j);
				if (((ElementDeJeuStatique)c).estDedans(e.getX(), e.getY()) || ((ElementDeJeuStatique)c).estDedans(e.getX() + (double)e.getTaille(), e.getY()) || ((ElementDeJeuStatique)c).estDedans(e.getX(), e.getY() + (double)e.getTaille()) || ((ElementDeJeuStatique)c).estDedans(e.getX() + (double)e.getTaille(), e.getY() + (double)e.getTaille())) {
					if (c instanceof MurFer && destruction) {
						c = new Fond(((ElementDeJeuStatique)c).x, ((ElementDeJeuStatique)c).y, ((ElementDeJeuStatique)c).taille);
						ligne.set(j, c);
						this.cases.set(i, ligne);
					}

					return (ElementDeJeuStatique)c;
				}
			}
		}

		return null;
	}

	public Char getCharDans(ElementDeJeu e) {
		Iterator var3 = this.ennemis.iterator();

		Char c;
		do {
			if (!var3.hasNext()) {
				return null;
			}

			c = (Char)var3.next();
		} while(!c.estDedans(e.getX(), e.getY()) && !c.estDedans(e.getX() + (double)e.getTaille(), e.getY()) && !c.estDedans(e.getX(), e.getY() + (double)e.getTaille()) && !c.estDedans(e.getX() + (double)e.getTaille(), e.getY() + (double)e.getTaille()));

		return c;
	}

	public void lireFichier(String nom) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(nom));
			String ligne = reader.readLine();
			String[] splited = ligne.split("=");
			if (splited[0].equals("tailleCase")) {
				this.tailleCase = Integer.parseInt(splited[1]);
			} else {
				this.tailleCase = 20;
			}

			ligne = reader.readLine();
			int n = 0;
			int m = 0;
			splited = ligne.split("=");
			if (splited[0].equals("map")) {
				String[] splited2 = splited[1].split("x");
				n = Integer.parseInt(splited2[0]);
				m = Integer.parseInt(splited2[1]);
			}

			this.N = n;
			this.M = m;
			int nbEnnemis = 0;

			int i;
			for(i = 0; nbEnnemis < n; i += this.tailleCase) {
				ArrayList<ElementDeJeuStatique> ligneCase = new ArrayList();
				ligne = reader.readLine();
				splited = ligne.split(";");
				int j = 0;

				for(int x = 0; j < m; x += this.tailleCase) {
					if (splited[j].equals("1")) {
						ligneCase.add(j, new MurBrique((double)x, (double)(this.tailleCase * (n - 1) - i), this.tailleCase));
					} else if (splited[j].equals("0")) {
						ligneCase.add(j, new Fond((double)x, (double)(this.tailleCase * (n - 1) - i), this.tailleCase));
					} else if (splited[j].equals("2")) {
						ligneCase.add(j, new MurFer((double)x, (double)(this.tailleCase * (n - 1) - i), this.tailleCase));
					} else {
						ligneCase.add(j, new Vegetation((double)x, (double)(this.tailleCase * (n - 1) - i), this.tailleCase));
					}

					++j;
				}

				this.cases.add(nbEnnemis, ligneCase);
				++nbEnnemis;
			}

			Collections.reverse(this.cases);
			ligne = reader.readLine();
			splited = ligne.split("=");
			if (splited[0].equals("monTank")) {
				splited = splited[1].split(";");
				this.monChar = new Char((double)(Integer.parseInt(splited[0]) * this.tailleCase + 2), (double)(Integer.parseInt(splited[1]) * this.tailleCase + 2), (double)Integer.parseInt(splited[2]), this.tailleCase - 4);
			} else {
				this.monChar = new Char((double)(this.tailleCase + 1), (double)(this.tailleCase + 1), 100.0, this.tailleCase - 2);
			}

			ligne = reader.readLine();
			splited = ligne.split("=");
			if (splited[0].equals("balles")) {
				splited = splited[1].split(";");
				this.vitesseBalle = (double)Integer.parseInt(splited[0]);
				this.nbMaxBalles = Integer.parseInt(splited[1]);
			} else {
				this.vitesseBalle = 500.0;
				this.nbMaxBalles = 10;
			}

			//AVIONS
			ligne = reader.readLine();
			splited = ligne.split("=");
			if (splited[0].equals("avion")) {
				splited = splited[1].split(";");
				this.avion = new Avion(this.tailleCase, Double.parseDouble(splited[0]), Float.parseFloat(splited[1]), this);
			}

			//OISEAUX
			ligne = reader.readLine();
			splited = ligne.split("=");
			if (splited[0].equals("oiseaux")) {
				this.nbOiseaux = Integer.parseInt(splited[1]);
			}
			for(nbEnnemis = 0; nbEnnemis < this.nbOiseaux; ++nbEnnemis) {
				Oiseau o = new Oiseau(0.0, 0.0, this.tailleCase);
				this.setRandomCoord(o);
				this.oiseaux.add(o);
			}

			//PHOENIX :
			ligne = reader.readLine();
			splited = ligne.split("=");
			if (splited[0].equals("phoenixes")) {
				this.nbPhoenix = Integer.parseInt(splited[1]);
			}
			for(nbEnnemis = 0; nbEnnemis < this.nbPhoenix; ++nbEnnemis) {
				Phoenix p = new Phoenix(0.0, 0.0, this.tailleCase);
				this.setRandomCoord(p);
				this.phoenixes.add(p);
			}


			//SCORE
			ligne = reader.readLine();
			splited = ligne.split("=");
			if (splited[0].equals("score")) {
				splited = splited[1].split(";");
				this.scoreEnnemi = Integer.parseInt(splited[0]);
				this.scoreOiseau = Integer.parseInt(splited[1]);
			}

			//VIES
			ligne = reader.readLine();
			splited = ligne.split("=");
			if (splited[0].equals("vies")) {
				this.vie = Integer.parseInt(splited[1]);
			} else {
				this.vie = 1;
			}

			//TirEnnemis
			ligne = reader.readLine();
			splited = ligne.split("=");
			if (splited[0].equals("tirEnnemis")) {
				this.tempsTirMax = (float)Integer.parseInt(splited[1]);
			} else {
				this.tempsTirMax = 5.0F;
			}

			this.tempsTir = this.tempsTirMax;
			nbEnnemis = Integer.parseInt(reader.readLine().split("=")[1]);

			for(i = 0; i < nbEnnemis; ++i) {
				splited = reader.readLine().split(";");
				if (splited[3].equals("1")) {
					this.ennemis.add(new CharEnnemi1(Double.parseDouble(splited[0]) * (double)this.tailleCase + 2.0, Double.parseDouble(splited[1]) * (double)this.tailleCase + 2.0, Double.parseDouble(splited[2]), this.tailleCase - 4, this));
				} else {
					this.ennemis.add(new CharEnnemi2(Double.parseDouble(splited[0]) * (double)this.tailleCase + 2.0, Double.parseDouble(splited[1]) * (double)this.tailleCase + 2.0, Double.parseDouble(splited[2]), this.tailleCase - 4, this));
				}
			}
		} catch (IOException var12) {
			var12.printStackTrace();
		}

	}

	public boolean end() {
		return this.finPartie;
	}
}
