package Modele;

import Controler.*;

public class ElementDeJeuMouvement extends ElementDeJeu {
	private double vitesse;
	protected int dirrection; // 1 : droite, 2 : gauche, 3 : haut, 4 : bas


	public ElementDeJeuMouvement(double x, double y, double vitesse, int taille) {
		super(x, y, taille);
		this.vitesse = vitesse;
		dirrection = 1;
	}

	public void deplacerHaut() {
		y += vitesse * WorldRenderer.DELTA();
		dirrection = 3;
	}

	public void deplacerBas() {
		y -= vitesse * WorldRenderer.DELTA();
		dirrection = 4;
	}

	public void deplacerDroite() {
		x += vitesse * WorldRenderer.DELTA();
		dirrection = 1;
	}

	public void deplacerGauche() {
		x -= vitesse * WorldRenderer.DELTA();
		dirrection = 2;
	}

	public int getDirrection() {
		return dirrection;
	}

	protected double getVitesse() {
		return vitesse;
	}
}
