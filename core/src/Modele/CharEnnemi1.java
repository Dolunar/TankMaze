package Modele;

	public class CharEnnemi1 extends CharEnnemi {
		private World world;

		public CharEnnemi1(double x, double y, double vitesse, int taille, World world) {
			super(x, y, vitesse, taille);
			this.world = world;
		}

		public void avancer(Char joueur) {
			if (joueur.getX() > this.x) {
				this.deplacerDroite();
				if (this.world.estDansMur(this)) {
					this.deplacerGauche();
				}
			} else if (joueur.getX() + (double)joueur.getTaille() < this.x) {
				this.deplacerGauche();
				if (this.world.estDansMur(this)) {
					this.deplacerDroite();
				}
			} else if (joueur.getY() + (double)joueur.getTaille() > this.y) {
				this.deplacerHaut();
				if (this.world.estDansMur(this)) {
					this.deplacerBas();
				}
			} else {
				this.deplacerBas();
				if (this.world.estDansMur(this)) {
					this.deplacerHaut();
				}
			}

		}
	}
