package Modele;

public class Balle extends ElementDeJeuLineaire {
	

	private int dirrection;
	private boolean gentille;
	
	public Balle(double x, double y, double vitessex, double vitessey, int taille, boolean gentille) {
		super(x, y, vitessex, vitessey, taille);
		if(vitessex > 0)
			dirrection = 1;
		else if(vitessex < 0)
			dirrection = 2;
		else if(vitessey > 0)
			dirrection = 3;
		else 
			dirrection = 4;
		this.gentille = gentille;
	}
	
	
	public int getDirrection() {
		return dirrection;
	}
	
	public boolean gentille() {
		return gentille;
	}

}
