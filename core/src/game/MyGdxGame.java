package game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import Controler.WorldRenderer;
import Modele.World;
import Vue.*;

public class MyGdxGame extends Game {
	SpriteBatch batch;

	public MyGdxGame() {
	}

	public void create() {
		this.batch = new SpriteBatch();
		Gdx.gl.glClear(16640);
		Gdx.gl.glClearColor(1.0F, 1.0F, 1.0F, 1.0F);
		World modele = new World("core/assets/config.txt");
		WorldRenderer controler = new WorldRenderer(modele, this);
		TankScreen vue = new TankScreen(controler, this);
		Gdx.input.setInputProcessor(controler);
		this.setScreen(vue);
	}

	public void dispose() {
		this.batch.dispose();
	}
}